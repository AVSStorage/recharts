const fs = require('fs').promises
const path = require('path')
const { randomIntFromInterval } = require('../utils')

const sendJsonFile = (jsonFile) => async (req, res, next) => {
  try {
    const data = await fs.readFile(path.resolve(__dirname, jsonFile), 'utf-8')
    res.json(JSON.parse(data))
  } catch (e) {
    next(e)
  }
}

const generateRandomStatisticsData = (data) => {
  return data.map((item) => ({
    ...item,
    uv: randomIntFromInterval(1000, 5000),
    pv: randomIntFromInterval(3000, 6000),
    amt: randomIntFromInterval(4000, 6000)
  }))
}

const sendRandomData = async (req, res, next) => {
  const filePath = req.query.dataset
    ? '../../data/bar.json'
    : '../../data/example.json'
  try {
    let data = await fs.readFile(path.resolve(__dirname, filePath), 'utf-8')
    data = JSON.parse(data)
    data[1] = generateRandomStatisticsData(data[1])
    res.json(data)
  } catch (e) {
    next(e)
  }
}

exports.sendRandomData = sendRandomData
exports.sendJsonFile = sendJsonFile
