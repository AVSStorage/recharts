const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')
const { sendJsonFile, sendRandomData } = require('./routes/helpers')

app.use(cors())
app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.get('/', sendJsonFile('../../data/population.json'))

app.get('/scatter', sendJsonFile('../../data/scatter.json'))
app.get('/pie', sendJsonFile('../../data/pie.json'))
app.get('/radial', sendJsonFile('../../data/radial.json'))

app.get('/area', sendRandomData)

app.get('/line', sendRandomData)

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
