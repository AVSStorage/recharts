import React from 'react'
import {
  RadialBarChart,
  RadialBar,
  Legend,
  ResponsiveContainer
} from 'recharts'
import H2 from '../../components/H2'
import Spinner from '../../components/Spinner'
import useChartData from '../hooks/useChartData'

const style = {
  top: '50%',
  right: 0,
  transform: 'translate(0, -50%)',
  lineHeight: '35px',
  fontSize: '25px'
}

const RadialBarChartComponent = () => {
  const [data, isLoading, error] = useChartData('radial')

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return 'Error'
  }

  return (
    <div style={{ height: '100vh' }}>
      <H2>Radial Bar Chart</H2>
      <div style={{ width: '40vw', height: '600px' }}>
        <ResponsiveContainer width="100%" height="100%">
          <RadialBarChart
            cx="50%"
            cy="50%"
            innerRadius="10%"
            outerRadius="90%"
            barSize={20}
            data={data}
          >
            <RadialBar
              minAngle={15}
              label={{
                position: 'insideStart',
                fill: '#000',
                fontWeight: 'bold'
              }}
              background
              clockWise
              dataKey="uv"
            />
            <Legend
              iconSize={10}
              layout="vertical"
              verticalAlign="middle"
              wrapperStyle={style}
            />
          </RadialBarChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default RadialBarChartComponent
