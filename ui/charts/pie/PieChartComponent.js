import React from 'react'
import {
  PieChart,
  Pie,
  Cell,
  ResponsiveContainer,
  Tooltip,
  Legend
} from 'recharts'
import { renderCustomizedLabel } from '../parts/CustomizedLabel'
import useChartData from '../hooks/useChartData'
import Spinner from '../../components/Spinner'
import H2 from '../../components/H2'

const COLORS = ['#0088FE', '#00C49F', '#001e82', '#5a1991', '#5ea740', '#FFBB28', '#FF8042', '#bcda07']

const legendStyle = {
  lineHeight: '35px',
  fontSize: '25px'
}

const PieChartComponent = () => {
  const [data, isLoading, error] = useChartData('pie')

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return 'Error'
  }

  return (
    <div style={{ height: '100vh' }}>
      <H2>Pie Chart</H2>
      <div style={{ width: '40vw', height: '600px' }}>
        <ResponsiveContainer>
          <PieChart>
            <Legend wrapperStyle={legendStyle} />
            <Tooltip />
            <Pie
              data={data}
              cx="50%"
              cy="50%"
              labelLine={false}
              label={renderCustomizedLabel}
              outerRadius={250}
              fill="#8884d8"
              dataKey="value"
            >
              {data.map((entry, index) => (
                <Cell
                  strokeWidth="3"
                  key={`cell-${index}`}
                  fill={COLORS[index % COLORS.length]}
                />
              ))}
            </Pie>
          </PieChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default PieChartComponent
