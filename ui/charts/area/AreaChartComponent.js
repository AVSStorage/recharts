import React, { useState } from 'react'
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer
} from 'recharts'
import useChartData from '../hooks/useChartData'
import Axis from '../parts/Axis'
import { CustomTooltip } from '../parts/CustomTooltip'
import Spinner from '../../components/Spinner'
import Button from '../../components/Button'
import H2 from '../../components/H2'

const axis = Axis({
  xAxisProps: { dataKey: 'name', tick: { stroke: 'white', strokeWidth: 1 } },
  yAxisProps: { tick: { stroke: 'white', strokeWidth: 1 } }
})

const AreaChartComponent = () => {
  const [colors, setColors] = useState(['#FFC2C2', '#92A6DD', '#DEE8FF'])
  const [isDataValid, setDataInvalidate] = useState(false)
  const [data, isLoading, error] = useChartData('area', isDataValid)

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return 'Error'
  }

  const refresh = () => setDataInvalidate(!isDataValid)

  return (
    <div style={{ height: '100vh' }}>
      <H2>Area Chart</H2>
      <Button onClick={refresh}>Refresh</Button>
      <div style={{ width: '60vw', height: '600px' }}>
        <ResponsiveContainer>
          <AreaChart
            data={data}
            margin={{
              top: 10,
              right: 30,
              left: 0,
              bottom: 0
            }}
          >
            {axis}
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip content={<CustomTooltip />} />
            <Area
              onClick={(e) => {
                setColors(['red', '#92A6DD', '#DEE8FF'])
              }}
              type="monotone"
              baseLine={8}
              activeDot={{ stroke: 'red', strokeWidth: 2, r: 10 }}
              dataKey="uv"
              stackId="1"
              stroke="#8884d8"
              fill={colors[0]}
            />
            <Area
              onClick={(e) => {
                setColors(['#FFC2C2', 'red', '#DEE8FF'])
              }}
              type="monotone"
              activeDot={{ stroke: 'white', strokeWidth: 2, r: 10 }}
              dataKey="pv"
              stackId="1"
              stroke="#82ca9d"
              fill={colors[1]}
            />
            <Area
              onClick={(e) => {
                setColors(['#FFC2C2', '#92A6DD', 'red'])
              }}
              type="monotone"
              activeDot={{ stroke: 'black', strokeWidth: 2, r: 10 }}
              dataKey="amt"
              stackId="1"
              stroke="#ffc658"
              fill={colors[2]}
            />
          </AreaChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default AreaChartComponent
