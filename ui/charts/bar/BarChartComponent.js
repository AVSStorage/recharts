import React from 'react'
import {
  BarChart,
  Bar,
  CartesianGrid,
  Legend,
  ResponsiveContainer
} from 'recharts'
import useChartData from '../hooks/useChartData'
import Spinner from '../../components/Spinner'
import Axis from '../parts/Axis'
import H2 from '../../components/H2'
import { useNavigate } from 'react-router-dom'
import { prepareSearchParam, getURLSearchParams } from '../../utils'
import Button from '../../components/Button'
import Label from '../../components/Label'
import DropDown from '../../components/DropDown'
import Row from '../../components/Row'
import { renderLegendText } from '../parts/Legend'

const axis = Axis({
  xAxisProps: { dataKey: 'name', tick: { stroke: 'white', strokeWidth: 1 } },
  yAxisProps: { tick: { stroke: 'white', strokeWidth: 1 } }
})
const chartMargin = {
  top: 5,
  right: 30,
  left: 20,
  bottom: 5
}

const BarChartComponent = () => {
  const navigate = useNavigate()
  const { dataset: queryParam } = getURLSearchParams()
  const path = queryParam ? `line/?dataset=${queryParam}` : 'line'

  const [data, isLoading, error] = useChartData(path, queryParam)

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return 'Error'
  }

  return (
    <div style={{ height: '100vh' }}>
      <div style={{ width: '60vw', height: '600px' }}>
        <H2>Bar Chart</H2>
        <Row>
          <Label>Select dataset:</Label>
          <DropDown
            onSelect={(value) =>
              navigate({
                pathname: window.location.pathname,
                search: `?dataset=${prepareSearchParam(value)}`
              })
            }
            options={[
              { value: 'select', name: 'Select value' },
              { value: 'dataset_2', name: 'Dataset 2' }
            ]}
          />
          <Button
            onClick={() => {
              navigate({
                pathname: window.location.pathname
              })
            }}
          >
            Reset
          </Button>
        </Row>
        <ResponsiveContainer>
          <BarChart
            data={data}
            margin={chartMargin}
          >
            <CartesianGrid strokeDasharray="3 3" />
            {axis}
            <Legend margin={10} formatter={renderLegendText} />
            <Bar dataKey="pv" fill="#84bb28" />
            <Bar dataKey="uv" fill="#007478" />
          </BarChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default BarChartComponent
