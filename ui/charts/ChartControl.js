import React from 'react'
import { useNavigate } from 'react-router-dom'
import Button from '../components/Button'
import { getURLSearchParams, prepareSearchParam } from '../utils'
import styles from './ChartControl.less'

const ChartControl = ({ mode, next, children }) => {
  const navigate = useNavigate()
  const { mode: queryParam } = getURLSearchParams()

  if (queryParam && queryParam !== prepareSearchParam(mode)) {
    return null
  }

  return (
    <div>
      <Button
        type={'primary'}
        onClick={() => {
          if (next) {
            navigate({
              pathname: window.location.pathname,
              search: `?mode=${prepareSearchParam(next)}`
            })
          } else {
            navigate({ pathname: window.location.pathname })
          }
        }}
      >
        Change mode
      </Button>
      <div className={styles.wrapper}>{children}</div>
    </div>
  )
}

export default React.memo(ChartControl)
