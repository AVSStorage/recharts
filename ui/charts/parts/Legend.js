import React from 'react'

export const renderLegendText = (value, entry) => {
  const { color } = entry

  return <span style={{ color, fontWeight: 'bold', fontSize: '24px' }}>{value}</span>
}
