import React from 'react'
import styles from './CustomTooltip.less'

export const CustomTooltip = ({ active, payload, label }) => {
  if (active && payload && payload.length) {
    return (
      <div className={styles.wrapper}>
        <p>{`${label} : ${payload[0].value}`}</p>
      </div>
    )
  }

  return null
}
