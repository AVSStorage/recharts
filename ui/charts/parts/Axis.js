import React from 'react'
import { XAxis, YAxis } from 'recharts'

const Axis = ({ xAxisProps, yAxisProps }) => {
  console.log(xAxisProps, yAxisProps)
  return (
    <>
      <XAxis {...xAxisProps} />
      <YAxis {...yAxisProps} />
    </>
  )
}

export default Axis
