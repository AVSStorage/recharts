import React from 'react'

export const CustomizedDot = (props) => {
  const { cx, cy, value } = props

  if (value < 2500) {
    return (
      <svg x={cx - 10} y={cy - 10} width={20} height={20} viewBox="0 0 24 24">
        <path
          fill="#fff"
          d="M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M7,10L12,15L17,10H7Z"
        />
      </svg>
    )
  }

  return (
    <svg x={cx - 10} y={cy - 10} width={20} height={20} viewBox="0 0 24 24">
      <path
        fill="#fff"
        d="M12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22M17,14L12,9L7,14H17Z"
      />
    </svg>
  )
}

export const CustomizedDot2 = (props) => {
  const { cx, cy, value } = props

  if (value < 2500) {
    return (
      <svg x={cx - 10} y={cy - 10} width={20} height={20} viewBox="0 0 24 24">
        <path
          fill="#fff"
          d="M16.59,5.59L18,7L12,13L6,7L7.41,5.59L12,10.17L16.59,5.59M16.59,11.59L18,13L12,19L6,13L7.41,11.59L12,16.17L16.59,11.59Z"
        />
      </svg>
    )
  }

  return (
    <svg x={cx - 10} y={cy - 10} width={20} height={20} viewBox="0 0 24 24">
      <path
        fill="#fff"
        d="M7.41,18.41L6,17L12,11L18,17L16.59,18.41L12,13.83L7.41,18.41M7.41,12.41L6,11L12,5L18,11L16.59,12.41L12,7.83L7.41,12.41Z"
      />
    </svg>
  )
}
