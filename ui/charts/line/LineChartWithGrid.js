import React, { useState } from 'react'
import { CHART_MODES } from '../../utils'
import Axis from '../parts/Axis'
import { Line, CartesianGrid, ReferenceLine } from 'recharts'
import LineChartTemplate from './LineChartTemplate'
import { CustomizedLabel } from '../parts/CustomizedLabel'

const axis = Axis({
  xAxisProps: {
    dataKey: 'name',
    tick: { stroke: 'lightgray', strokeWidth: 1 }
  },
  yAxisProps: { tick: { stroke: 'lightgray', strokeWidth: 1 } }
})

const LineChartWithGrid = () => {
  const [refAreas, setRefAreas] = useState({
    refAreaLeft: '',
    refAreaRight: ''
  })

  return (
    <LineChartTemplate
      chartProps={{
        onMouseMove: (e) => {
          setRefAreas({ refAreaLeft: e.activeLabel })
        }
      }}
      title='With Grid'
      mode={CHART_MODES.MODE_GRID}
      next={CHART_MODES.MODE_LEGEND}
    >
      <CartesianGrid strokeDasharray={'3 3'} />
      {axis}
      <ReferenceLine
        x={refAreas.refAreaLeft}
        strokeWidth={3}
        label={<CustomizedLabel stroke="lightGray" value="Selected" />}
        stroke="red"
        strokeDasharray="3 3"
      />
      <ReferenceLine
        segment={[
          { y: 3800, x: 'Page A' },
          { x: 'Page C', y: 1800 }
        ]}
        stroke="#000"
        label="Segment"
      />
      <ReferenceLine
        segment={[
          { y: 1800, x: 'Page D' },
          { x: 'Page F', y: 4500 }
        ]}
        stroke="#fff"
      />
      <Line
        type="monotone"
        dataKey="pv"
        strokeWidth={3}
        stroke="red"
        activeDot={{ r: 8 }}
      />
      <Line type="monotone" dataKey="uv" strokeWidth={3} stroke="black" />
    </LineChartTemplate>
  )
}

export default LineChartWithGrid
