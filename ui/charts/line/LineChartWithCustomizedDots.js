import React from 'react'
import { Line, CartesianGrid } from 'recharts'
import { CHART_MODES } from '../../utils'
import { CustomizedDot, CustomizedDot2 } from '../parts/CustomizedDot'
import LineChartTemplate from './LineChartTemplate'
import Axis from '../parts/Axis'

const axis = Axis({
  xAxisProps: {
    dataKey: 'name',
    tick: { stroke: 'lightGray' },
    margin: { top: 10 },
    tickMargin: 10
  },
  yAxisProps: { tick: { stroke: 'lightGray', strokeWidth: 1 }, tickMargin: 10 }
})

const LineWithCustomizedDots = () => {
  return (
    <LineChartTemplate
      title='With custom dot'
      mode={CHART_MODES.MODE_CUSTOM_DOT}
    >
      <CartesianGrid strokeDasharray={'3 3'} />
      {axis}
      <Line type="monotone" dataKey="pv" dot={<CustomizedDot />} />
      <Line type="monotone" dataKey="uv" dot={<CustomizedDot2 />} />
    </LineChartTemplate>
  )
}

export default LineWithCustomizedDots
