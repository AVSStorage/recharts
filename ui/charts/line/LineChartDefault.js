import React from 'react'
import LineChartTemplate from './LineChartTemplate'
import { CHART_MODES } from '../../utils'
import { CartesianGrid, Legend, Tooltip, Line } from 'recharts'
import Axis from '../parts/Axis'
import { renderLegendText } from '../parts/Legend'

const axis = Axis({
  xAxisProps: {
    dataKey: 'name',
    tick: { stroke: 'lightgray', strokeWidth: 1 }
  },
  yAxisProps: { tick: { stroke: 'lightgray', strokeWidth: 1 } }
})

const LineChartDefault = () => {
  return (
    <LineChartTemplate next={CHART_MODES.MODE_AXIS}>
      {axis}
      <CartesianGrid strokeDasharray={'3 3'} />
      <Legend formatter={renderLegendText} margin={{ top: 20 }} />
      <Tooltip />
      <Line
        type="monotone"
        dataKey="pv"
        strokeWidth={3}
        stroke="red"
        activeDot={{ r: 8 }}
      />
      <Line type="monotone" dataKey="uv" strokeWidth={3} stroke="black" />
    </LineChartTemplate>
  )
}

export default LineChartDefault
