import React, { useState } from 'react'
import { CHART_MODES } from '../../utils'
import {
  Line,
  Tooltip,
  CartesianGrid,
  ReferenceArea,
  XAxis,
  YAxis
} from 'recharts'
import Axis from '../parts/Axis'
import ChatControl from '../ChartControl'
import LineChartComponent from '../line/LineChart'
import Spinner from '../../components/Spinner'
import H2 from '../../components/H2'
import useChartData from '../hooks/useChartData'
import { CustomTooltip } from '../parts/CustomTooltip'
import Button from '../../components/Button'
import Row from '../../components/Row'
import { prepareSearchParam, getURLSearchParams } from '../../utils'

const initialState = {
  left: 'dataMin',
  right: 'dataMax',
  refAreaLeft: '',
  refAreaRight: '',
  top: 'dataMax+1',
  bottom: 'dataMin-1',
  top2: 'dataMax+20',
  bottom2: 'dataMin-20',
  animation: true
}

const getAxisYDomain = (initialData, from, to, ref, offset) => {
  const refData = initialData.slice(from - 1, to)
  let [bottom, top] = [refData[0][ref], refData[0][ref]]
  refData.forEach((d) => {
    if (d[ref] > top) top = d[ref]
    if (d[ref] < bottom) bottom = d[ref]
  })

  return [(bottom | 0) - offset, (top | 0) + offset]
}

const zoom = (setAreas, areas, data) => {
  let { refAreaLeft, refAreaRight } = areas

  if (refAreaLeft === refAreaRight || refAreaRight === '') {
    setAreas((state) => ({
      ...state,
      refAreaLeft: '',
      refAreaRight: ''
    }))
    return
  }

  if (refAreaLeft > refAreaRight) {
    [refAreaLeft, refAreaRight] = [refAreaRight, refAreaLeft]
  }

  const from = data.findIndex((item) => item.name === refAreaLeft)
  const to = data.findIndex((item) => item.name === refAreaRight)
  const [bottom, top] = getAxisYDomain(data, from, to, 'pv', 1)

  setAreas({
    refAreaLeft: '',
    refAreaRight: '',
    left: refAreaLeft,
    right: refAreaRight,
    bottom,
    top
  })
}

const zoomOut = (setAreas) => {
  setAreas((state) => ({
    ...state,
    refAreaLeft: '',
    refAreaRight: '',
    left: 'dataMin',
    right: 'dataMax',
    top: 'dataMax+1',
    bottom: 'dataMin'
  }))
}

const LineChartWithTootip = () => {
  const [areas, setAreas] = useState(initialState)
  const { mode: queryParam } = getURLSearchParams()

  const [data, isLoading, error] = useChartData('line')
  const hasQueryParam =
    queryParam === prepareSearchParam(CHART_MODES.MODE_TOOLTIP)

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return 'Error'
  }

  const onZoomChange = () => zoomOut(setAreas)
  const onMouseUp = () => zoom(setAreas, areas, data)
  const onMouseDown = (e) => setAreas({ ...areas, refAreaLeft: e.activeLabel })
  const onMouseMove = (e) =>
    areas.refAreaLeft && setAreas({ ...areas, refAreaRight: e.activeLabel })

  return (
    <>
      {hasQueryParam && (
        <Row>
          <Button onClick={onZoomChange}>Zoom Out</Button>
        </Row>
      )}
      <ChatControl
        mode={CHART_MODES.MODE_TOOLTIP}
        next={CHART_MODES.MODE_CUSTOM_DOT}
      >
        <H2>With Tooltip</H2>
        <LineChartComponent
          chartProps={{
            onMouseDown,
            onMouseMove,
            onMouseUp,
            data
          }}
        >
          <XAxis
            tick={{ stroke: 'lightGray' }}
            allowDataOverflow
            dataKey="name"
            domain={[areas.left, areas.right]}
          />
          <YAxis
            tick={{ stroke: 'lightGray' }}
            allowDataOverflow
            domain={[areas.bottom, areas.top]}
          />
          <Tooltip content={<CustomTooltip />} />
          <CartesianGrid strokeDasharray={'3 3'} />
          <Line
            type="monotone"
            dataKey="pv"
            strokeWidth={3}
            stroke="red"
            activeDot={{ r: 8 }}
          />
          <Line type="monotone" dataKey="uv" strokeWidth={3} stroke="black" />
          {areas.refAreaLeft && areas.refAreaRight && (
            <ReferenceArea
              x1={areas.refAreaLeft}
              x2={areas.refAreaRight}
              strokeOpacity={0.3}
            />
          )}
        </LineChartComponent>
      </ChatControl>
    </>
  )
}

export default LineChartWithTootip
