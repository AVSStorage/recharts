import React, { useState } from 'react'
import { Legend, Line, CartesianGrid } from 'recharts'
import { CHART_MODES } from '../../utils'
import LineChartTemplate from './LineChartTemplate'
import Axis from '../parts/Axis'
import { renderLegendText } from '../parts/Legend'

const axis = Axis({
  xAxisProps: {
    dataKey: 'name',
    tick: { stroke: 'lightGray', strokeWidth: 1 }
  },
  yAxisProps: { tick: { stroke: 'lightGray', strokeWidth: 1 } }
})

const LineChartWithLegend = () => {
  const [opacity, changeOpacity] = useState({
    uv: 1,
    pv: 1,
    amt: 1
  })

  const handleMouseEnter = (o) => {
    const { dataKey } = o

    changeOpacity({ ...opacity, [dataKey]: 3 })
  }

  const handleMouseLeave = (o) => {
    const { dataKey } = o

    changeOpacity({ ...opacity, [dataKey]: 1 })
  }

  return (
    <LineChartTemplate
      title="With Legend"
      mode={CHART_MODES.MODE_LEGEND}
      next={CHART_MODES.MODE_TOOLTIP}
    >
      <CartesianGrid strokeDasharray={'3 3'} />
      {axis}
      <Legend
        formatter={renderLegendText}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        margin={{ top: 80 }}
      />
      <Line
        strokeWidth={opacity.amt}
        type="monotone"
        dataKey="amt"
        stroke="blue"
      />
      <Line
        strokeWidth={opacity.pv}
        type="monotone"
        dataKey="pv"
        stroke="red"
        activeDot={{ r: 8 }}
      />
      <Line
        strokeWidth={opacity.uv}
        type="monotone"
        dataKey="uv"
        stroke="black"
      />
    </LineChartTemplate>
  )
}

export default LineChartWithLegend
