import React from 'react'
import { Line } from 'recharts'
import { CHART_MODES } from '../../utils'
import LineChartTemplate from './LineChartTemplate'
import Axis from '../parts/Axis'
import { CustomizedLabel } from '../parts/CustomizedLabel'

const axis = Axis({
  xAxisProps: { dataKey: 'uv', tick: { stroke: 'lightgray', strokeWidth: 1 } },
  yAxisProps: { tick: { stroke: 'lightgray', strokeWidth: 1 } }
})

const LineChartWithAxis = () => {
  return (
    <LineChartTemplate
      title="With Axis"
      mode={CHART_MODES.MODE_AXIS}
      next={CHART_MODES.MODE_GRID}
    >
      {axis}
      <Line
        type="monotone"
        dataKey="amt"
        strokeWidth={2}
        label={<CustomizedLabel stroke="lightgray" />}
        stroke="red"
        activeDot={{ r: 8 }}
      />
      <Line type="monotone" dataKey="uv" strokeWidth={1} stroke="black" />
    </LineChartTemplate>
  )
}

export default LineChartWithAxis
