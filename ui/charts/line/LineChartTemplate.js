import React from 'react'
import ChartControl from '../ChartControl'
import LineChartComponent from './LineChart'
import H2 from '../../components/H2'
import useChartData from '../hooks/useChartData'
import Spinner from '../../components/Spinner'

const LineChartTemplate = ({ mode, next, title, children, chartProps }) => {
  const path = mode ? `line/?mode=${mode}` : 'line'
  const [data, isLoading, error] = useChartData(path)

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return 'Error'
  }

  return (
    <ChartControl mode={mode} next={next}>
      <H2>{title}</H2>
      <LineChartComponent chartProps={{ ...chartProps, data }}>
        {children}
      </LineChartComponent>
    </ChartControl>
  )
}

export default LineChartTemplate
