import React from 'react'
import { LineChart, ResponsiveContainer } from 'recharts'

const LineChartComponent = ({ children, chartProps }) => {
  return (
    <div style={{ height: '100vh' }}>
      <div style={{ width: '60vw', height: '600px' }}>
        <ResponsiveContainer>
          <LineChart
            {...chartProps}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            {children}
          </LineChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default LineChartComponent
