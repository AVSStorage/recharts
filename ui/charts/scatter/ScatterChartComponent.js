import React from 'react'
import {
  ScatterChart,
  Scatter,
  LabelList,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer
} from 'recharts'
import { CustomizedDot2 } from '../parts/CustomizedDot'
import useChartData from '../hooks/useChartData'
import Spinner from '../../components/Spinner'
import Axis from '../parts/Axis'
import H2 from '../../components/H2'

const axis = Axis({
  xAxisProps: {
    type: 'number',
    dataKey: 'x',
    unit: 'cm',
    tick: { stroke: 'white', strokeWidth: 1 }
  },
  yAxisProps: {
    type: 'number',
    dataKey: 'y',
    unit: 'kg',
    tick: { stroke: 'white', strokeWidth: 1 }
  }
})

const chartMargin = {
  top: 20,
  right: 20,
  bottom: 20,
  left: 20
}

const ScatterChartComponent = () => {
  const [data, isLoading, error] = useChartData('scatter')

  if (isLoading) {
    return <Spinner />
  }

  if (error) {
    return 'Error'
  }

  return (
    <div style={{ height: '100vh' }}>
      <H2>Scatter Chart</H2>
      <div style={{ width: '70vw', height: '700px' }}>
        <ResponsiveContainer>
          <ScatterChart
            margin={chartMargin}
          >
            <CartesianGrid />
            {axis}
            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
            <Scatter
              name="A school"
              data={data}
              shape={<CustomizedDot2 />}
              fill="#fff"
            >
              <LabelList
                dataKey="x"
                fill="#84bb28"
                fontSize="16px"
                fontWeight="bold"
                position="bottom"
              />
            </Scatter>
          </ScatterChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default ScatterChartComponent
