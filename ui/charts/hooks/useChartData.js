import { useState, useEffect } from 'react'
import api from '../../api'

const useChartData = (path = '', refresh) => {
  const [data, setData] = useState([{}])
  const [isLoading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    const getChartData = async () => {
      try {
        setLoading(true)
        const data = await api.get(path)
        setData(data[1])
      } catch (e) {
        setError(e)
      } finally {
        setLoading(false)
      }
    }
    getChartData()
  }, [refresh])

  return [data, isLoading, error]
}

export default useChartData
