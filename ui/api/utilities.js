export const preformatResponse = async (response) => {
  const responseContentType = response.headers.get('content-type')

  if (responseContentType.includes('application/json')) {
    response = await response.json()
  }

  return response
}

export const prepareResponse = async (response) => {
  if (!response.ok) {
    throw new Error('Error')
  }

  response = await preformatResponse(response)

  return response
}
