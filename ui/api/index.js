import { prepareResponse } from './utilities'

const API_URL = process.env.API_URL

const createRequestWithBody = (method) => async (path, body, options) => {
  let response = await fetch(`${API_URL}/${path}`, { body, method, ...options })

  response = await prepareResponse(response)

  return response
}

const api = {
  get: async (path = '') => {
    let response = await fetch(`${API_URL}/${path}`)

    response = await prepareResponse(response)

    return response
  },
  post: createRequestWithBody('POST'),
  put: createRequestWithBody('PUT'),
  delete: createRequestWithBody('DELETE')
}

export default api
