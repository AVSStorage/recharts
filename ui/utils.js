import { useLocation } from 'react-router'

export const getURLSearchParams = () => {
  const { search } = useLocation()
  const queryParams = new URLSearchParams(search)

  return Object.fromEntries(queryParams.entries())
}

export const prepareSearchParam = (param) => param.toLowerCase().replace('_', '-')

export const CHART_MODES = {
  MODE_AXIS: 'AXIS',
  MODE_GRID: 'GRID',
  MODE_LEGEND: 'LEGEND',
  MODE_TOOLTIP: 'TOOLTIP',
  MODE_CUSTOM_DOT: 'CUSTOM_DOT'
}
