import React from 'react'
import './App.less'
import Sidebar from './components/Sidebar'
import NotFound from './pages/NotFound'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import LineChartPage from './pages/LineChartPage'
import Layout from './components/Layout'
import AreaChartPage from './pages/AreaChartPage'
import ScatterChartPage from './pages/ScatterChartPage'
import PieChartPage from './pages/PieChartPage'
import BarChartPage from './pages/BarChartPage'
import WelcomePage from './pages/WelcomePage'

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<WelcomePage />} />
        <Route
          exact
          path="/linear"
          element={
            <Layout>
              <LineChartPage />
            </Layout>
          }
        />
        <Route exact path="/area" element={<AreaChartPage />} />
        <Route exact path="/scatter" element={<ScatterChartPage />} />
        <Route exact path="/pie" element={<PieChartPage />} />
        <Route exact path="/bar" element={<BarChartPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
