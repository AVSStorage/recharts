import BarChartComponent from '../charts/bar/BarChartComponent'
import React from 'react'
import Layout from '../components/Layout'

const BarChartPage = () => {
  return (
    <Layout>
      <BarChartComponent />
    </Layout>
  )
}

export default BarChartPage
