import ScatterChartComponent from '../charts/scatter/ScatterChartComponent'
import React from 'react'
import Layout from '../components/Layout'

const ScatterChartPage = () => {
  return (
  <Layout>
    <ScatterChartComponent />
    </Layout>
  )
}

export default ScatterChartPage
