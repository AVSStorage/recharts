import React from 'react'
import LineChartWithAxis from '../charts/line/LineChartWithAxis'
import LineChartWithGrid from '../charts/line/LineChartWithGrid'
import LineChartWithLegend from '../charts/line/LineChartWithLegend'
import LineChartWithTootip from '../charts/line/LineChartWithTooltip'
import LineWithCustomizedDots from '../charts/line/LineChartWithCustomizedDots'
import { getURLSearchParams } from '../utils'
import LineChartDefault from '../charts/line/LineChartDefault'

const LineChartPage = () => {
  const { mode } = getURLSearchParams()

  if (!mode) {
    return (
      <LineChartDefault/>
    )
  }

  return (
    <>
      <LineChartWithAxis />
      <LineChartWithGrid />
      <LineChartWithLegend />
      <LineChartWithTootip />
      <LineWithCustomizedDots />
    </>
  )
}

export default LineChartPage
