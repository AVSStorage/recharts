import React from 'react'
import Layout from '../components/Layout'
import PieChartComponent from '../charts/pie/PieChartComponent'
import RadialBarChartComponent from '../charts/pie/RadialBarChartComponent'
import Row from '../components/Row'

const PieChartPage = () => {
  return (
    <Layout>
      <Row>
        <PieChartComponent />
        <RadialBarChartComponent />
      </Row>
    </Layout>
  )
}

export default PieChartPage
