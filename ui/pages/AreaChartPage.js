import AreaChartComponent from '../charts/area/AreaChartComponent'
import React from 'react'
import Layout from '../components/Layout'

const AreaChartPage = () => {
  return (
    <Layout>
      <AreaChartComponent />
    </Layout>
  )
}

export default AreaChartPage
