import React from 'react'
import Layout from '../components/Layout'
import H2 from '../components/H2'
import Menu from '../components/Menu'

const WelcomePage = () => {
  return (
        <Layout>
            <H2>Welcome Page</H2>
            <Menu/>
        </Layout>
  )
}

export default WelcomePage
