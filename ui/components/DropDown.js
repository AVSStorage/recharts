import React from 'react'
import styles from './DropDown.less'

const DropDown = ({ onSelect, options }) => {
  return (
    <select
      className={styles.dropDown}
      onChange={(e) => onSelect(e.target.value)}
    >
      {options.map((option, index) => (
        <option value={option.value} key={index}>
          {option.name}
        </option>
      ))}
    </select>
  )
}

export default DropDown
