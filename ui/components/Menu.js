import React from 'react'
import { Link } from 'react-router-dom'
import styles from './Menu.less'

const Menu = () => {
  return (
        <nav>
          <ul className={styles.menu}>
            <li>
              <Link to="/linear">Linear</Link>
            </li>
            <li>
              <Link to="/bar">Bar</Link>
            </li>
            <li>
              <Link to="/area">Area</Link>
            </li>
            <li>
              <Link to="/scatter">Scatter</Link>
            </li>
            <li>
              <Link to="/pie">Pie</Link>
            </li>
          </ul>
        </nav>
  )
}

export default Menu
