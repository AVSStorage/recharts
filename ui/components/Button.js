import React from 'react'
import styles from './Button.less'

const Button = ({ onClick, disabled, type = 'primary', children }) => {
  return (
    <button
      onClick={onClick}
      disabled={disabled}
      className={`${styles.reset} ${styles[type]}`}
    >
      {children}
    </button>
  )
}

export default Button
