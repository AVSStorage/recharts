import React, { useState } from 'react'
import styles from './Sidebar.less'
import Button from './Button'
import Menu from './Menu'

const svgStyle = { width: '24px', height: '24px' }

const Sidebar = () => {
  const [isOpen, toggle] = useState(false)

  const wrapperClassName = isOpen
    ? `${styles.wrapper} ${styles.wrapperOpen}`
    : `${styles.wrapper} ${styles.wrapperClose}`

  const asideClassName = isOpen ? styles.open : styles.close
  const onButtonClick = () => toggle(!isOpen)
  const buttonType = isOpen ? 'primary' : 'default'

  return (
    <div className={wrapperClassName}>
      <Button type={buttonType} onClick={onButtonClick}>
        <svg style={svgStyle} viewBox="0 0 24 24">
          <path
            fill="currentColor"
            d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z"
          />
        </svg>
      </Button>
      <aside className={asideClassName}>
        <Menu />
      </aside>
    </div>
  )
}

export default Sidebar
