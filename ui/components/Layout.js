import React from 'react'
import Sidebar from './Sidebar'
import styles from './Layout.less'

const Layout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <Sidebar />
      <div className={styles.wrapper}>{children}</div>
    </div>
  )
}

export default Layout
