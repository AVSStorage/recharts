import React from 'react'
import './H2.less'

const H2 = ({ children }) => {
  return <h2>{children}</h2>
}

export default H2
