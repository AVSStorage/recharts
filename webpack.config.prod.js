const path = require('path')

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const { DefinePlugin } = require('webpack')
const HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
  title: 'Production',
  template: path.resolve(__dirname, '/public/index.html'),
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  mode: 'production',
  entry: path.resolve(__dirname, 'ui', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: './',
    clean: true
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.less$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: { publicPath: '../' }
          },
          'css-loader',
          'less-loader'
        ]
      }
    ]
  },

  plugins: [
    new MiniCssExtractPlugin(),
    HTMLWebpackPluginConfig,
    new DefinePlugin({
      'process.env.API_URL': JSON.stringify('http://localhost:3000')
    })
  ],

  resolve: {
    extensions: ['.js', '.jsx', '.css', '*']
  },

  performance: {
    hints: false
  },

  stats: {
    errorDetails: true,
    warnings: true,
    colors: true
  }

}
